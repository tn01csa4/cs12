import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavconService {

  display_navbar : boolean;

  constructor() {
    this.display_navbar = true;
   }

  show(){
    this.display_navbar = true; 
  }

  hide(){
    this.display_navbar = false;
  }

  togglenavbar(){
    this.display_navbar = !this.display_navbar;
  }

}
