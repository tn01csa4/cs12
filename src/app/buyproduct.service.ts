import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BuyproductService {

  constructor(private db: AngularFireDatabase) {
   }
  
  addtocart(a,b,c,d){
     //a => title
     //b => price
     //c => productname
     //var total = 1 * b;
     var email = localStorage.getItem('k');
     localStorage.setItem('hider', 'true'); 
     this.db.object('/shoppingcart/'+email+'/'+c).update({ title: a, price: b, quantity: 1, productname: c, category: d, total: b*1});
  }

}
