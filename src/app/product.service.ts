import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root' 
})
export class ProductService {

  constructor(private db: AngularFireDatabase) { }

  create(product){
    return  this.db.object('/products/'+product.category+'/'+product.productname).update({ category: product.category, title: product.title, price: product.price, imageUrl: product.imageUrl, productname: product.productname, description: product.description});
  }     

  //function for getting the list of books 
  getBooks() {
    return this.db.list('/products/Books').valueChanges();
  }
  //function for getting the list of phones
  getPhones() {
    return this.db.list('/products/Phones').valueChanges();
  }
  //funcion for getting list fashion
  getFashion() {
    return this.db.list('/products/Fashion').valueChanges();
  }
  //funcion for getting list office
  getOffice() {
    return this.db.list('/products/Office').valueChanges();
  }
  //funcion for getting list tools
  getTools() {
    return this.db.list('/products/Tools').valueChanges();
  }
}
