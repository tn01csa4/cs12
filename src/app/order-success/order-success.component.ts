import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AngularFireDatabase } from '@angular/fire/database';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { NavconService } from '../navcon.service';

@Component({
  selector: 'app-order-success',
  templateUrl: './order-success.component.html',
  styleUrls: ['./order-success.component.css']
})
export class OrderSuccessComponent implements OnInit {

  private user: firebase.User;
  orders$: Observable<any>;

  constructor(private db: AngularFireDatabase, private router: Router, private afauth:AngularFireAuth, private nav:NavconService) {

    //saving the current user to the localstorage
    afauth.authState.subscribe(user => {this.user = user; localStorage.setItem('currentmail', user.email);}); //displayName

    //reformatting the user email. replacing . by *
    var sam = localStorage.getItem("currentmail");
    var res = sam.replace(/\./g, '*');
    localStorage.setItem('k', res);
    var currentuser = localStorage.getItem('k');
    this.orders$ = this.db.list('/orders/'+currentuser).valueChanges();
   }

  loadorders(orderid){
    var t = orderid;
    localStorage.setItem('userorder', t);
    this.router.navigate(['order-product']);
  }

  ngOnInit() {
    this.nav.show();
  }

}
