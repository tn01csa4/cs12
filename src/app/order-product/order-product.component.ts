import { AngularFireDatabase } from '@angular/fire/database';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { $ } from 'protractor';
import { NavconService } from '../navcon.service';

@Component({
  selector: 'app-order-product',
  templateUrl: './order-product.component.html',
  styleUrls: ['./order-product.component.css']
})
export class OrderProductComponent implements OnInit {

  productdisplayer$:Observable<any>;

  constructor(private db:AngularFireDatabase, private nav:NavconService) {
    var currentuser = localStorage.getItem('k');
    var orderid = localStorage.getItem('userorder');
    this.productdisplayer$ = this.db.list('/orders/'+currentuser+'/'+orderid+'/products').valueChanges();
   }

  ngOnInit() {
    this.nav.show();
  }

}
