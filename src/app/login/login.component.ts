import { Component, OnInit } from '@angular/core';
//import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from './../auth.service';
import { NavconService } from '../navcon.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
	constructor(private auth : AuthService, private nav:NavconService) {
	}

	login() {
		this.auth.login();
	}

	ngOnInit() {
		this.nav.show();
	}
}
 


