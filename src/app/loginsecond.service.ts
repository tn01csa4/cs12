import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginsecondService {

  display_content : Boolean;

  constructor() {
    //this.display_content = true;
   }

   show(){
     this.display_content = true;
   }

   hide(){
     this.display_content = false;
   }

   togglecontent(){
     this.display_content = !this.display_content;
   }
  
}
