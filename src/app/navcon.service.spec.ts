import { TestBed } from '@angular/core/testing';

import { NavconService } from './navcon.service';

describe('NavconService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NavconService = TestBed.get(NavconService);
    expect(service).toBeTruthy();
  });
});
