import { LoginsecondService } from './loginsecond.service';
import { ProductService } from './product.service';
import { CategoryService } from './category.service';
import { environment } from './../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
//import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';

import { AuthService } from './auth.service';
import { AuthGuard } from './auth-guard.service';
import { UserService } from './user.service';

import { ProductstoolsComponent } from './productstools/productstools.component';
import { ProductsComponent } from './admin/products/products.component';
import { OrdersComponent } from './admin/orders/orders.component';
import { CheckOutComponent } from './check-out/check-out.component';
import { OrderSuccessComponent } from './order-success/order-success.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { HomeComponent } from './home/home.component';
import { ProductFormComponent } from './admin/product-form/product-form.component';
import { OrderProductComponent } from './order-product/order-product.component';
import { ViewProductComponent } from './view-product/view-product.component';
import { OrdersSellerComponent } from './admin/orders-seller/orders-seller.component';
import { ViewproductsComponent } from './admin/viewproducts/viewproducts.component';
import { NavconService } from './navcon.service';
import { LogintwoComponent } from './logintwo/logintwo.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    ProductstoolsComponent,
    ProductsComponent,
    OrdersComponent,
    CheckOutComponent,
    OrderSuccessComponent,
    ShoppingCartComponent,
    HomeComponent,
    ProductFormComponent,
    OrderProductComponent,
    ViewProductComponent,
    OrdersSellerComponent,
    ViewproductsComponent,
    LogintwoComponent,
  ],
  imports: [
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    BrowserModule,
    FormsModule,
    NgbModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),

    RouterModule.forRoot([
      {path:'' ,component:HomeComponent},
      {path:'login' ,component:LoginComponent},
      {path:'products' ,component:ProductstoolsComponent},

      {path:'shopping-cart' ,component:ShoppingCartComponent},
      {path:'view-product', component:ViewProductComponent},  
      {path:'check-out' ,component:CheckOutComponent, canActivate:[AuthGuard]},
      {path:'order-success' ,component:OrderSuccessComponent, canActivate:[AuthGuard]},
      {path:'order-product' ,component:OrderProductComponent, canActivate:[AuthGuard]},
      
      //{path:'seller/orders' ,component:OrdersComponent, canActivate:[AuthGuard]},
      //{path:'seller/orders-seller' ,component:OrdersSellerComponent, canActivate:[AuthGuard]},
      //{path:'seller/orders-seller/viewproducts' ,component:ViewproductsComponent, canActivate:[AuthGuard]},

      //{path:'admin/products' ,component:ProductsComponent, canActivate:[AuthGuard]},
      //{path:'admin/products/new' ,component:ProductFormComponent, canActivate:[AuthGuard]},

      {path:'seller/orders' ,component:OrdersComponent},
      {path:'seller/orders-seller' ,component:OrdersSellerComponent},
      {path:'seller/orders-seller/viewproducts' ,component:ViewproductsComponent},

      {path:'admin/products' ,component:ProductsComponent},
      {path:'admin/products/new' ,component:ProductFormComponent},

      {path:'login/elv' ,component:LogintwoComponent}
      ])
  ],
  providers: [
  AuthService,
  AuthGuard,
  UserService,
  CategoryService,
  ProductService,
  NavconService,
  LoginsecondService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
