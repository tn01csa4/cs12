import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';
import { NavconService } from '../navcon.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
	
	constructor(public auth:AuthService, public navbartoggle : NavconService) { 
  	}

  	logout(){
  		this.auth.logout();
  	}

}
