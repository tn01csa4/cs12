import { AuthService } from './../auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { BuyproductService } from './../buyproduct.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Location } from '@angular/common';
import { NavconService } from '../navcon.service';

@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.css']
})
export class ViewProductComponent implements OnInit {

  productfetch$: Observable<any>;
  private user: firebase.User;

  constructor(private db:AngularFireDatabase,private location:Location, private nav: NavconService,private buyproductservice:BuyproductService,
    public afauth:AngularFireAuth, public auth:AuthService) {
    var product_name = localStorage.getItem('name');
    var product_category = localStorage.getItem('category');
    this.productfetch$ = this.db.list('/products/'+product_category+'/', ref => ref.orderByChild('productname').equalTo(product_name)).valueChanges();
   }


   buyfunction(title,price,productname,category){
    
    //saving the current user to the localstorage
    this.afauth.authState.subscribe(user => {this.user = user; localStorage.setItem('currentmail', user.email);}); //displayName

    //reformatting the user email. replacing . by *
    var sam = localStorage.getItem("currentmail");
    var res = sam.replace(/\./g, '*');
    localStorage.setItem('k', res);

    var a = title;
    var b = price;
    var c = productname;
    var d = category;

    if (category == "Books"){
      var boughtbooks = category;
      localStorage.setItem('b_book', boughtbooks);
    }
    else if (category == "Phones"){
      var boughtphones = category;
      localStorage.setItem('b_phone', boughtphones);
    }
    else if (category == "Tools"){
      var boughttools = category;
      localStorage.setItem('b_tool', boughttools);
    }
    else if (category == "Office"){
      var boughtoffice = category;
      localStorage.setItem('b_office', boughtoffice);
    }
    else{
      var boughtfashion = category;
      localStorage.setItem('b_fashion', boughtfashion);
    }

    this.buyproductservice.addtocart(a,b,c,d);
    alert("Added To Cart!");
    //create another entry
    //this.buyproductservice.addanotherchild(a,b,c);
    //alert("Added To other child!");
  }
  
  back(){
    this.location.back();
  }

  ngOnInit() {
    this.nav.show();
  }

}
