import { LoginsecondService } from './../loginsecond.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NavconService } from '../navcon.service';

@Component({
  selector: 'app-logintwo',
  templateUrl: './logintwo.component.html',
  styleUrls: ['./logintwo.component.css']
})
export class LogintwoComponent implements OnInit {

  constructor(private router: Router, private contenttoggler:LoginsecondService, public navshow:NavconService) { }

  logincheck(formvalue){
    var x = formvalue.id;
    var y = formvalue.pwd;

    if(x == "admin" || x == "ADMIN" || x == "Admin"){
      if(y == "admin"){
        this.contenttoggler.show();
        this.router.navigate(['/admin/products']);
      }
      else{
        alert("Error");
      }
    }
    else if(x == "shipping" || x == "Shipping" || x == "SHIPPING"){
      if(y == "shipping"){
        this.contenttoggler.show();
        this.router.navigate(['/seller/orders']);
      }
      else{
        alert("Error");
      }
    }
    else{
      alert("Error");
    }
      
  }

  ngOnInit() {
    this.navshow.hide();
  }

}
