import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductstoolsComponent } from './productstools.component';

describe('ProductstoolsComponent', () => {
  let component: ProductstoolsComponent;
  let fixture: ComponentFixture<ProductstoolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductstoolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductstoolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
