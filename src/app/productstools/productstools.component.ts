import { BuyproductService } from './../buyproduct.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from './../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { ProductService } from './../product.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { NavconService } from '../navcon.service';

@Component({
  selector: 'app-productstools',
  templateUrl: './productstools.component.html',
  styleUrls: ['./productstools.component.css']
})
export class ProductstoolsComponent implements OnInit {
  private user: firebase.User;
  //variable to store data from firebase
  productdata$: Observable<any>;

  constructor(private productService: ProductService, private db: AngularFireDatabase, public afauth:AngularFireAuth, 
    private buyproductservice:BuyproductService, public auth:AuthService, private nav:NavconService) {
    let type = localStorage.getItem('type');
    //console.log(type);
    if (type == "Tools")
      this.productdata$ = this.productService.getTools();
    else if(type == "Books")
      this.productdata$ = this.productService.getBooks();
    else if(type == "Phones")
      this.productdata$ = this.productService.getPhones();
    else if(type == "Office")
      this.productdata$ = this.productService.getOffice();
    else
      this.productdata$ = this.productService.getFashion();
    
    //commented this part because of issue with logged out users not being able to view products
    /*
    //saving the current user to the localstorage
    afauth.authState.subscribe(user => {this.user = user; localStorage.setItem('currentmail', user.email);}); //displayName

    //reformatting the user email. replacing . by *
    var sam = localStorage.getItem("currentmail");
    var res = sam.replace(/\./g, '*');
    localStorage.setItem('k', res);*/

    //the below variable contains the user name, this will be used to create carts and display them to the user  
    //var d = localStorage.getItem('k');
    //console.log(d);
   }

  buyfunction(title,price,productname,category){
    
    //saving the current user to the localstorage
    this.afauth.authState.subscribe(user => {this.user = user; localStorage.setItem('currentmail', user.email);}); //displayName

    //reformatting the user email. replacing . by *
    var sam = localStorage.getItem("currentmail");
    var res = sam.replace(/\./g, '*');
    localStorage.setItem('k', res);

    var a = title;
    var b = price;
    var c = productname;
    var d = category;

    /*
    if (category == "Books"){
      var boughtbooks = category;
      localStorage.setItem('b_book', boughtbooks);
    }
    else if (category == "Phones"){
      var boughtphones = category;
      localStorage.setItem('b_phone', boughtphones);
    }
    else if (category == "Tools"){
      var boughttools = category;
      localStorage.setItem('b_tool', boughttools);
    }
    else if (category == "Office"){
      var boughtoffice = category;
      localStorage.setItem('b_office', boughtoffice);
    }
    else{
      var boughtfashion = category;
      localStorage.setItem('b_fashion', boughtfashion);
    }
    */

    this.buyproductservice.addtocart(a,b,c,d);
    alert("Added To Cart!");
    //create another entry
    //this.buyproductservice.addanotherchild(a,b,c);
    //alert("Added To other child!");
  } 

  sort(){
    var product_category = localStorage.getItem('type');
    this.productdata$ = this.db.list('/products/'+product_category+'/', ref => ref.orderByChild('price')).valueChanges();
  }

  viewthisproduct(category,name){
    var n = name;
    var c = category;
    localStorage.setItem('name', n);
    localStorage.setItem('category', c);
  }

  ngOnInit() {
    this.nav.show();
  }

}
