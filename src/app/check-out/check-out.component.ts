import { Router } from '@angular/router';
import { AngularFireDatabase } from '@angular/fire/database';
import { Component, OnInit } from '@angular/core';
import { NavconService } from '../navcon.service';

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.css']
})
export class CheckOutComponent implements OnInit {

  constructor(private db: AngularFireDatabase, private router: Router, private nav:NavconService) {
    
   }

  checkout(cart){

    // this part takes a snapshot of the cart for checking out
    /* 
    var ref = this.db.database.ref('/shoppingcart/'+currentuser);
    ref.once("value").then(function(snapshot) {
      var shoppingcart = snapshot.val();
      localStorage.setItem('cart', JSON.stringify(shoppingcart));
    })*/
    var cartfinal = JSON.parse(localStorage.getItem('cart'));

    //checking if user has bought item for assigning order to seller
    /*
    var b = localStorage.getItem('b_book');
    var p = localStorage.getItem('b_phone');
    var f = localStorage.getItem('b_fashion');
    var t = localStorage.getItem('b_tool');
    var o = localStorage.getItem('b_office');*/

    //getting current date in the appropriate format
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    var formatteddate = dd + '/' + mm + '/' + yyyy;

    //getting the time in milliseconds to use as the order key
    var d = new Date();
    var currentdate = d.getTime();

    //fetch the current user from the localstorage
    var currentuser = localStorage.getItem('k');

    //console.log(JSON.parse(localStorage.getItem('cart')));

    //now we save the order to firebase and clear the cart and alert the user and redirect user to the home page
    this.db.object('/orders/'+currentuser+'/'+currentdate+'/').update({id:currentdate, date:formatteddate, products:cartfinal, address:cart.address, name:cart.username});
    alert("Order Placed Successfully!");

    // 23 march update
    //seller has been removed form the system replaced by shipping
    this.db.object('/ordershipping/'+currentdate+'/').update({id:currentdate, date:formatteddate, products:cartfinal, address:cart.address, city:cart.city, state:cart.state, name:cart.username});


    //this section will assign the order to the appropriate sellers

    //section A

    /*
    if(cart.state == "Maharashtra" || cart.state == "JandK" || cart.state == "Punjab" || cart.state == "Rajasthan" 
    || cart.state == "Gujarat" || cart.state == "Goa" || cart.state == "Karnataka" || cart.state == "Kerala" 
    || cart.state == "MP" )
    {
      if(b == "Books"){
        this.db.object('/seller/sellerAbook/'+currentdate+'/').update({id:currentdate, date:formatteddate, products:cartfinal, address:cart.address, city:cart.city, state:cart.state, name:cart.username});
      }

      if(p == "Phones"){
        this.db.object('/seller/sellerAphone/'+currentdate+'/').update({id:currentdate, date:formatteddate, products:cartfinal, address:cart.address, city:cart.city, state:cart.state, name:cart.username});
      }

      if(f == "Fashion"){
        this.db.object('/seller/sellerAfashion/'+currentdate+'/').update({id:currentdate, date:formatteddate, products:cartfinal, address:cart.address, city:cart.city, state:cart.state, name:cart.username});
      }

      if(t == "Tools"){
        this.db.object('/seller/sellerAtools/'+currentdate+'/').update({id:currentdate, date:formatteddate, products:cartfinal, address:cart.address, city:cart.city, state:cart.state, name:cart.username});
      }

      if(o == "Office"){
        this.db.object('/seller/sellerAsupplies/'+currentdate+'/').update({id:currentdate, date:formatteddate, products:cartfinal, address:cart.address, city:cart.city, state:cart.state, name:cart.username});
      }
    }

    //section B
    if(cart.state == "HP" || cart.state == "Haryana" || cart.state == "Uttarakhand" || cart.state == "UP" 
    || cart.state == "Telangana" || cart.state == "Chhattisgarh" || cart.state == "AP" || cart.state == "Odisha" 
    || cart.state == "TN" )
    {
      if(b == "Books"){
        this.db.object('/seller/sellerBbook/'+currentdate+'/').update({id:currentdate, date:formatteddate, products:cartfinal, address:cart.address, city:cart.city, state:cart.state, name:cart.username});
      }

      if(p == "Phones"){
        this.db.object('/seller/sellerBphone/'+currentdate+'/').update({id:currentdate, date:formatteddate, products:cartfinal, address:cart.address, city:cart.city, state:cart.state, name:cart.username});
      }

      if(f == "Fashion"){
        this.db.object('/seller/sellerBfashion/'+currentdate+'/').update({id:currentdate, date:formatteddate, products:cartfinal, address:cart.address, city:cart.city, state:cart.state, name:cart.username});
      }

      if(t == "Tools"){
        this.db.object('/seller/sellerBtools/'+currentdate+'/').update({id:currentdate, date:formatteddate, products:cartfinal, address:cart.address, city:cart.city, state:cart.state, name:cart.username});
      }

      if(o == "Office"){
        this.db.object('/seller/sellerBsupplies/'+currentdate+'/').update({id:currentdate, date:formatteddate, products:cartfinal, address:cart.address, city:cart.city, state:cart.state, name:cart.username});
      }
    }

    //section C
    if(cart.state == "ArunachalP" || cart.state == "Assam" || cart.state == "Bihar" || cart.state == "Jharkhand" 
    || cart.state == "Manipur" || cart.state == "Meghalaya" || cart.state == "Mizoram" || cart.state == "Nagaland" 
    || cart.state == "Sikkim" || cart.state == "Tripura" || cart.state == "WB")
    {
      if(b == "Books"){
        this.db.object('/seller/sellerCbook/'+currentdate+'/').update({id:currentdate, date:formatteddate, products:cartfinal, address:cart.address, city:cart.city, state:cart.state, name:cart.username});
      }

      if(p == "Phones"){
        this.db.object('/seller/sellerCphone/'+currentdate+'/').update({id:currentdate, date:formatteddate, products:cartfinal, address:cart.address, city:cart.city, state:cart.state, name:cart.username});
      }

      if(f == "Fashion"){
        this.db.object('/seller/sellerCfashion/'+currentdate+'/').update({id:currentdate, date:formatteddate, products:cartfinal, address:cart.address, city:cart.city, state:cart.state, name:cart.username});
      }

      if(t == "Tools"){
        this.db.object('/seller/sellerCtools/'+currentdate+'/').update({id:currentdate, date:formatteddate, products:cartfinal, address:cart.address, city:cart.city, state:cart.state, name:cart.username});
      }

      if(o == "Office"){
        this.db.object('/seller/sellerCsupplies/'+currentdate+'/').update({id:currentdate, date:formatteddate, products:cartfinal, address:cart.address, city:cart.city, state:cart.state, name:cart.username});
      }
    }
    */

    this.db.object('/shoppingcart/'+currentuser).remove();
    this.router.navigate(['']);

    //localStorage.removeItem('cart');
    //localStorage.removeItem('b_book');
    //localStorage.removeItem('b_phone');
    //localStorage.removeItem('b_fashion');
    //localStorage.removeItem('b_tool');
    //localStorage.removeItem('b_office');

  }

  ngOnInit() {
    this.nav.show();
  }

}
