import { Component, OnInit } from '@angular/core';
import { NavconService } from '../navcon.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private nav:NavconService) { }
  
  typeistools(){
    localStorage.setItem('type', "Tools");
  }
  typeisbooks(){
    localStorage.setItem('type', "Books");
  }
  typeisfashion(){
    localStorage.setItem('type', "Fashion");
  }
  typeisphones(){
    localStorage.setItem('type', "Phones");
  }
  typeisoffice(){
    localStorage.setItem('type', "Office");
  }
  ngOnInit() {
    this.nav.show();
  }

}
