import { Location } from '@angular/common';
import { AuthService } from './../auth.service';
import { AngularFireDatabase, snapshotChanges } from '@angular/fire/database';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { NavconService } from '../navcon.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

  public show = localStorage.getItem('hider');

  shoppingcart$: Observable<any>;

  constructor(private db: AngularFireDatabase, public auth:AuthService, private location:Location, private nav: NavconService) {
    var mail = localStorage.getItem('k');
    this.shoppingcart$ = this.db.list('/shoppingcart/'+mail).valueChanges();

    //this part is experimental, here, i am taking a snapshot of the users shopping cart for pushing into the 
    //database later when the user decides to check out.
    /*var ref = this.db.database.ref("/shoppingcart/John Doe");
    ref.once("value").then(function(snapshot) {
      var k = snapshot.val();
      console.log(k);
    })*/

    var currentuser = localStorage.getItem('k');
    // this part takes a snapshot of the cart for checking out 
    var ref = this.db.database.ref('/shoppingcart/'+currentuser);
    ref.once("value").then(function(snapshot) {
      var shoppingcart = snapshot.val();
      // 3-4-19
      var addup = 0
      snapshot.forEach(function(baccha){
        addup += baccha.val().total;
      });
      
      let casted = String(addup);
      localStorage.setItem('carttotal', casted);
      //end of code added on 3-4-19
      //console.log(shoppingcart);
      localStorage.setItem('cart', JSON.stringify(shoppingcart));
    });

   }

  //used for decrementing the quantity
  decrementqty(productname, quantity, price){
    console.log("Decremented");
    var mail = localStorage.getItem('k');
    var h = productname;
    var newqty = quantity-1;
    this.db.object('/shoppingcart/'+mail+'/'+h).update({quantity: quantity-1, total : newqty*price});
    var currentuser = localStorage.getItem('k');
    // this part takes a snapshot of the cart for checking out 
    var ref = this.db.database.ref('/shoppingcart/'+currentuser);
    ref.once("value").then(function(snapshot) {
      var shoppingcart = snapshot.val();
      // 3-4-19
      var addup = 0
      snapshot.forEach(function(baccha){
        addup += baccha.val().total;
      });
      
      let casted = String(addup);
      localStorage.setItem('carttotal', casted);
      //end of code added on 3-4-19
      //console.log(shoppingcart);
      localStorage.setItem('cart', JSON.stringify(shoppingcart));
    })
  }

  //used for increasing the quantity
  incrementqty(productname, quantity, price){
    console.log("Incremented");
    var mail = localStorage.getItem('k');
    var h = productname;
    var newqty = quantity+1;
    this.db.object('/shoppingcart/'+mail+'/'+h).update({quantity: quantity+1, total : newqty*price});
    var currentuser = localStorage.getItem('k');
    // this part takes a snapshot of the cart for checking out 
    var ref = this.db.database.ref('/shoppingcart/'+currentuser);
    ref.once("value").then(function(snapshot) {
      var shoppingcart = snapshot.val();
      // 3-4-19
      var addup = 0
      snapshot.forEach(function(baccha){
        addup += baccha.val().total;
      });
      
      let casted = String(addup);
      localStorage.setItem('carttotal', casted);
      //end of code added on 3-4-19
      //console.log(shoppingcart);
      localStorage.setItem('cart', JSON.stringify(shoppingcart));
    })
  }
  
  //runs the clear cart button
  deletecart(){
    var mail = localStorage.getItem('k');
    this.db.object('/shoppingcart/'+mail).remove();
    //localStorage.removeItem('cart');
    //localStorage.removeItem('b_book');
    //localStorage.removeItem('b_phone');
    //localStorage.removeItem('b_tool');
    //localStorage.removeItem('b_office');
    //localStorage.removeItem('b_fashion');
    //localStorage.removeItem('hider');
    localStorage.removeItem('carttotal');
    this.location.back();
  }

  //runs the checkout button
  get carttotal(): any {
    return localStorage.getItem('carttotal');
  }

  ngOnInit() {
    this.nav.show();
  }

}
