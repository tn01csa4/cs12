import { LoginsecondService } from './../../loginsecond.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { ProductService } from './../../product.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { NavconService } from 'src/app/navcon.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  books$: Observable<any[]>;
  fashion$: Observable<any[]>;
  office$: Observable<any[]>;
  phones$: Observable<any[]>;
  tools$: Observable<any[]>;

  constructor(private productService: ProductService, private db: AngularFireDatabase, private nav:NavconService,
    public contenttoggler: LoginsecondService) {
    this.books$ = this.productService.getBooks();
    this.fashion$ = this.productService.getFashion();
    this.office$ = this.productService.getOffice();
    this.phones$ = this.productService.getPhones();
    this.tools$ = this.productService.getTools();
   }

  deletefunction(productname, category){
    var x = productname;
    var y = category;
    //console.log("Deleted", x);
    this.db.object("/products/"+y+"/"+x).remove();
    alert("Product was deleted.");
  }

  logout(){
    this.contenttoggler.hide();
  }

  ngOnInit() {
    this.nav.hide();
  }

}
