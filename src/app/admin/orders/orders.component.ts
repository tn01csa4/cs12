import { AngularFireDatabase } from '@angular/fire/database';
import { LoginsecondService } from './../../loginsecond.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { NavconService } from 'src/app/navcon.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  shipping$:Observable<any>;
  constructor(private router: Router, private nav:NavconService, public contenttoggler: LoginsecondService, private db: AngularFireDatabase) {
    this.shipping$ = this.db.list('/ordershipping/').valueChanges();
   }

  /*
  sellerfunction(sellerid){
    localStorage.setItem('sellerid', sellerid);
    this.router.navigate(['seller/orders-seller']);
  }*/

  showorder(orderid){
    localStorage.setItem('orderid', orderid);
    this.router.navigate(['seller/orders-seller/viewproducts']);
  }

  logout(){
    this.contenttoggler.hide();
  }

  ngOnInit() {
    this.nav.hide();
  }

}
