import { LoginsecondService } from './../../loginsecond.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { NavconService } from 'src/app/navcon.service';

@Component({
  selector: 'app-orders-seller',
  templateUrl: './orders-seller.component.html',
  styleUrls: ['./orders-seller.component.css']
})
export class OrdersSellerComponent implements OnInit {

  //selleri$:Observable<any>;
  shipping$:Observable<any>;

  constructor(private db: AngularFireDatabase, private router: Router, private nav:NavconService, 
    public contenttoggler: LoginsecondService) 
   {
    //var who = localStorage.getItem('sellerid');
    //this.selleri$ = this.db.list('/seller/'+who+'/').valueChanges();
    this.shipping$ = this.db.list('/ordershipping/').valueChanges();
   }

  fetchsellerproduct(orderid){
    localStorage.setItem('sellerorderid', orderid);
    this.router.navigate(['seller/orders-seller/viewproducts']);
  }

  logout(){
    this.contenttoggler.hide();
  }

  ngOnInit() {
    this.nav.hide();
  }

}
