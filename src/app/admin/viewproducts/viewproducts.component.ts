import { AngularFireDatabase } from '@angular/fire/database';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { NavconService } from 'src/app/navcon.service';

@Component({
  selector: 'app-viewproducts',
  templateUrl: './viewproducts.component.html',
  styleUrls: ['./viewproducts.component.css']
})
export class ViewproductsComponent implements OnInit {

  shipproduct$:Observable<any>;

  constructor(private db: AngularFireDatabase, private nav:NavconService) {
    /*var x = localStorage.getItem('sellerorderid');
    var y = localStorage.getItem('sellerid');
    if(y == "sellerAbook" || y == "sellerBbook" || y == "sellerCbook")
    {
      this.sellerproduct$ = this.db.list('/seller/'+y+'/'+x+'/products/', ref => ref.orderByChild('category').equalTo('Books')).valueChanges();
    }
      
    if(y == "sellerAfashion" || y == "sellerBfashion" || y == "sellerCfashion")
    {
      this.sellerproduct$ = this.db.list('/seller/'+y+'/'+x+'/products/', ref => ref.orderByChild('category').equalTo('Fashion')).valueChanges();
    }

    if(y == "sellerAsupplies" || y == "sellerBsupplies" || y == "sellerCsupplies")
    {
      this.sellerproduct$ = this.db.list('/seller/'+y+'/'+x+'/products/', ref => ref.orderByChild('category').equalTo('Office')).valueChanges();
    }
    
    if(y == "sellerAphone" || y == "sellerBphone" || y == "sellerCphone")
    {
      this.sellerproduct$ = this.db.list('/seller/'+y+'/'+x+'/products/', ref => ref.orderByChild('category').equalTo('Phones')).valueChanges();
    }

    if(y == "sellerAtools" || y == "sellerBtools" || y == "sellerCAtools")
    {
      this.sellerproduct$ = this.db.list('/seller/'+y+'/'+x+'/products/', ref => ref.orderByChild('category').equalTo('Tools')).valueChanges();
    }*/

    var x = localStorage.getItem('orderid');
    this.shipproduct$ = this.db.list('/ordershipping/'+x+'/products').valueChanges();

   }

  ngOnInit() {
    this.nav.hide();
  }

}
