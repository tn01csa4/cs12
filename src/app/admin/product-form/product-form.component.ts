import { LoginsecondService } from './../../loginsecond.service';
import { Router } from '@angular/router';
import { ProductService } from './../../product.service';
import { CategoryService } from './../../category.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { NavconService } from 'src/app/navcon.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {
  categories$: Observable<any[]>;

  constructor(
    private router: Router,
    private categoryService: CategoryService,
    private productService: ProductService,
    private nav:NavconService, public contenttoggler: LoginsecondService) {
    this.categories$ = categoryService.getCategories();
    
  }

  save(product) {
    this.productService.create(product);
    this.router.navigate(['/admin/products']);
  }

  logout(){
    this.contenttoggler.hide();
  }

  ngOnInit() {
    this.nav.hide();
  }

}
